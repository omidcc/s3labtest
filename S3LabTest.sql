USE [master]
GO
/****** Object:  Database [S3LabTestDB]    Script Date: 9/14/2017 4:41:09 PM ******/
CREATE DATABASE [S3LabTestDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'S3LabTestDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\S3LabTestDB.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'S3LabTestDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\S3LabTestDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [S3LabTestDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [S3LabTestDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [S3LabTestDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [S3LabTestDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [S3LabTestDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [S3LabTestDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [S3LabTestDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [S3LabTestDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [S3LabTestDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [S3LabTestDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [S3LabTestDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [S3LabTestDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [S3LabTestDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [S3LabTestDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [S3LabTestDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [S3LabTestDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [S3LabTestDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [S3LabTestDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [S3LabTestDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [S3LabTestDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [S3LabTestDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [S3LabTestDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [S3LabTestDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [S3LabTestDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [S3LabTestDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [S3LabTestDB] SET  MULTI_USER 
GO
ALTER DATABASE [S3LabTestDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [S3LabTestDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [S3LabTestDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [S3LabTestDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [S3LabTestDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [S3LabTestDB]
GO
/****** Object:  Table [dbo].[Languages]    Script Date: 9/14/2017 4:41:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Languages](
	[LanguageId] [int] NOT NULL,
	[LanguageName] [varchar](50) NOT NULL,
	[LanguageShortName] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[LanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Levels]    Script Date: 9/14/2017 4:41:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Levels](
	[LevelId] [int] NOT NULL,
	[LevelName] [varchar](50) NOT NULL,
	[LevelShortName] [varchar](20) NOT NULL,
	[LevelDescription] [varchar](200) NOT NULL,
	[TradeId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[LevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Syllabus]    Script Date: 9/14/2017 4:41:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Syllabus](
	[SyllabusId] [int] IDENTITY(1,1) NOT NULL,
	[SyllabusName] [varchar](50) NOT NULL,
	[TradeId] [int] NOT NULL,
	[LevelId] [int] NOT NULL,
	[SyllabusDocUrl] [varchar](200) NOT NULL,
	[TestPlanUrl] [varchar](200) NOT NULL,
	[DevelopmentOfficer] [varchar](100) NOT NULL,
	[Manager] [varchar](100) NOT NULL,
	[UploadBy] [int] NOT NULL,
	[UploadDate] [datetime] NOT NULL,
	[ActiveDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SyllabusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SyllabusLanguages]    Script Date: 9/14/2017 4:41:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SyllabusLanguages](
	[SyllabusLanguageId] [int] IDENTITY(1,1) NOT NULL,
	[SyllabusId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SyllabusLanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Trades]    Script Date: 9/14/2017 4:41:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Trades](
	[TradeId] [int] NOT NULL,
	[TradeCode] [varchar](50) NOT NULL,
	[TradeName] [varchar](200) NOT NULL,
	[Abbreviation] [varchar](50) NOT NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[TradeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Languages] ([LanguageId], [LanguageName], [LanguageShortName]) VALUES (1, N'English', N'EN')
INSERT [dbo].[Languages] ([LanguageId], [LanguageName], [LanguageShortName]) VALUES (2, N'Chinese', N'CH')
INSERT [dbo].[Languages] ([LanguageId], [LanguageName], [LanguageShortName]) VALUES (3, N'Thai', N'TH')
INSERT [dbo].[Languages] ([LanguageId], [LanguageName], [LanguageShortName]) VALUES (4, N'Tamil', N'TA')
INSERT [dbo].[Languages] ([LanguageId], [LanguageName], [LanguageShortName]) VALUES (5, N'Korean', N'KR')
INSERT [dbo].[Levels] ([LevelId], [LevelName], [LevelShortName], [LevelDescription], [TradeId]) VALUES (1, N'SEC(K)', N'SEC(K)', N'Description', 1)
INSERT [dbo].[Levels] ([LevelId], [LevelName], [LevelShortName], [LevelDescription], [TradeId]) VALUES (2, N'SATF', N'SATF', N'', 1)
SET IDENTITY_INSERT [dbo].[Syllabus] ON 

INSERT [dbo].[Syllabus] ([SyllabusId], [SyllabusName], [TradeId], [LevelId], [SyllabusDocUrl], [TestPlanUrl], [DevelopmentOfficer], [Manager], [UploadBy], [UploadDate], [ActiveDate], [Status]) VALUES (16, N'sdsad', 1, 2, N'2626-Master-CV-2 (1).pdf', N'2626-Master-CV-2 (2).pdf', N'sdsdsd', N'sdsdsd', 1, CAST(N'2017-09-14 16:38:32.007' AS DateTime), CAST(N'2017-09-14 04:37:25.107' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Syllabus] OFF
SET IDENTITY_INSERT [dbo].[SyllabusLanguages] ON 

INSERT [dbo].[SyllabusLanguages] ([SyllabusLanguageId], [SyllabusId], [LanguageId]) VALUES (56, 16, 1)
INSERT [dbo].[SyllabusLanguages] ([SyllabusLanguageId], [SyllabusId], [LanguageId]) VALUES (57, 16, 4)
INSERT [dbo].[SyllabusLanguages] ([SyllabusLanguageId], [SyllabusId], [LanguageId]) VALUES (58, 16, 3)
INSERT [dbo].[SyllabusLanguages] ([SyllabusLanguageId], [SyllabusId], [LanguageId]) VALUES (59, 16, 2)
SET IDENTITY_INSERT [dbo].[SyllabusLanguages] OFF
INSERT [dbo].[Trades] ([TradeId], [TradeCode], [TradeName], [Abbreviation], [Status]) VALUES (1, N'0001', N'Timber Formwork Installation', N'TFI', 1)
INSERT [dbo].[Trades] ([TradeId], [TradeCode], [TradeName], [Abbreviation], [Status]) VALUES (2, N'0002', N'Tower Crane Operation (Luffing Jib)', N'TCO (LJ)', 1)
ALTER TABLE [dbo].[Levels]  WITH CHECK ADD FOREIGN KEY([TradeId])
REFERENCES [dbo].[Trades] ([TradeId])
GO
ALTER TABLE [dbo].[Syllabus]  WITH CHECK ADD FOREIGN KEY([LevelId])
REFERENCES [dbo].[Levels] ([LevelId])
GO
ALTER TABLE [dbo].[Syllabus]  WITH CHECK ADD FOREIGN KEY([TradeId])
REFERENCES [dbo].[Trades] ([TradeId])
GO
ALTER TABLE [dbo].[SyllabusLanguages]  WITH CHECK ADD FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Languages] ([LanguageId])
GO
ALTER TABLE [dbo].[SyllabusLanguages]  WITH CHECK ADD FOREIGN KEY([SyllabusId])
REFERENCES [dbo].[Syllabus] ([SyllabusId])
GO
USE [master]
GO
ALTER DATABASE [S3LabTestDB] SET  READ_WRITE 
GO
