﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using TestMarketingSystem.Models;

namespace TestMarketingSystem.ViewModels
{
    public class SyllabusViewModel
    {
        public Syllabu Syllabus { get; set; }
        public List<int> SyllabusLanguages { get; set; }
        public List<HttpPostedFile> Files { get; set; }
        public string TradeName { get; set; }
        public string LevelName { get; set; }
        public List<string> LanguageShortNames { get; set; }

        public bool IsUploadAllowed { get; set; }
    }
}