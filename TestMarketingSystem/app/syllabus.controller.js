﻿(function () {
    angular.module("app").controller("SyllabusController", syllabusController).directive("validFile",
        function () {
            return {
                require: 'ngModel',
                link: function (scope, el, attrs, ngModel) {
                    el.bind('change',
                        function (event) {
                            var files = event.target.files;
                            for (var i = 0; i < files.length; i++) {
                                scope.$emit("seletedFile", { file: files[i] });
                            }
                            scope.$apply(function () {
                                ngModel.$setViewValue(el.val());
                                ngModel.$render();
                            });
                        });
                }
            }
        });

    function syllabusController($scope, $http) {
        var vm = this;
        var service = $http;
        vm.languages = [];
        vm.levels = [];
        vm.trades = [];
        vm.isSaveDisabled = true;
        vm.savedSuccessfully = false;
        vm.isUpdateDisabled = true;
        vm.selectedLanguageLists = [];
        vm.toggleSelection = function (languageId) {
            var idx = vm.selectedLanguageLists.indexOf(languageId);

            // is currently selected
            if (idx > -1) {
                vm.selectedLanguageLists.splice(idx, 1);
            }

            // is newly selected
            else {
                vm.selectedLanguageLists.push(languageId);
            }
            if (vm.selectedLanguageLists.length === 0) {
                vm.isSaveDisabled = true;
                vm.isUpdateDisabled = true;
            } else {
                vm.isSaveDisabled = false;
                vm.isUpdateDisabled = false;
            }
            console.log(vm.isSaveDisabled);
        }

        getAllLanguages();
        getAllLevels();
        getAllTrades();

        //selectListTab();
        vm.isListActive = false;
        vm.isCreateActive = true;
        vm.isListLoaded = false;
        vm.isChartVisible = false;
        vm.isUpdateVisible = false;
        vm.isSaveVisible = true;
        vm.selectListTab = function () {
            vm.isListActive = true;
            vm.isCreateActive = false;
            vm.isChartVisible = false;
            if (!vm.listLoaded) {
                vm.getAllSyllabus();
            }
        }
        vm.selectCreateTab = function () {
            vm.isListActive = false;
            vm.isCreateActive = true;
            vm.isChartVisible = false;
        }
        vm.selectChart = function () {
            vm.isListActive = false;
            vm.isCreateActive = false;
            vm.isChartVisible = true;
        }

        vm.selectedLevel = "";
        vm.selectedTrade = "";

        function getAllLanguages() {
            service.get("/api/getlanguages")
                .then(function (response) {
                    vm.languages = response.data;
                    console.log(vm.languages);
                });
        }

        function getAllLevels() {
            service.get("/api/getlevels")
                .then(function (response) {
                    vm.levels = response.data;
                    console.log(vm.levels);
                });
        }
        function getAllTrades() {
            service.get("/api/gettrades")
                .then(function (response) {
                    vm.trades = response.data;
                    console.log(vm.trades);
                });
        }

        vm.syllabusVm = {
            Syllabus: {
                SyllabusId: 0,
                SyllabusName: "",
                TradeId: 0,
                LevelId: 0,
                SyllabusDocUrl: "",
                TestPlanUrl: "",
                DevelopmentOfficer: "",
                Manager: "",
                UploadBy: 1,
                UploadDate: new Date(),
                ActiveDate: new Date(),
                Status: 1
            },
            SyllabusLanguages: vm.selectedLanguageLists,
            Files: new FormData(),
            TradeName: "",
            LevelName: "",
            LanguageShortNames: ""
        }

        vm.createSyllabus = function (syllabusForm) {
            var form = syllabusForm;
            form.$submitted = true;

            if (form.$valid) {
                form.$setPristine();
                form.$setUntouched();
                //get checkboxes value
                vm.syllabusVm.Syllabus.LevelId = vm.selectedLevel;
                vm.syllabusVm.Syllabus.TradeId = vm.selectedTrade;

                service.post("/api/syllabus", vm.syllabusVm)
                    .then(function (response) {
                            $scope.save();
                            vm.getAllSyllabus();
                        },
                        function (response) {

                        });
            } else {
                form.$valid = false;
            }
        }
        vm.updateSyllabus = function (syllabusForm, id) {
            var form = syllabusForm;
            form.$submitted = true;

            if (form.$valid) {
                form.$setPristine();
                form.$setUntouched();
                //get checkboxes value
                vm.syllabusVm.Syllabus.SyllabusId = id;
                vm.syllabusVm.Syllabus.LevelId = vm.selectedLevel;
                vm.syllabusVm.Syllabus.TradeId = vm.selectedTrade;
                vm.syllabusVm.SyllabusLanguages = vm.selectedLanguageLists;
                //update into the api
                service.post("/api/updatesyllabus", vm.syllabusVm)
                    .then(function (response) {
                            if (response.data.IsUploadAllowed) {
                                $scope.save();
                            }
                            vm.getAllSyllabus();
                            alert("Updated successfully");
                            vm.selectListTab();
                        },
                        function (response) {
                            console.log(response);
                        });
            }
            else {
                form.$valid = false;
            }
        }
        $scope.jsonData = {
            name: "Jignesh Trivedi",
            comments: "Multiple upload files"
        };
        $scope.files = [];
        $scope.$on("seletedFile", function (event, args) {
            $scope.$apply(function () {
                $scope.files.push(args.file);
                console.log($scope.files);
            });
        });
        $scope.save = function () {
            $http({
                method: 'POST',
                url: "/api/upload",
                headers: { 'Content-Type': undefined },

                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("model", angular.toJson(data.model));
                    for (var i = 0; i < data.files.length; i++) {
                        formData.append("file" + i, data.files[i]);
                    }
                    return formData;
                },
                data: { model: $scope.jsonData, files: $scope.files }
            }).then(function (data, status, headers, config) {
                    vm.savedSuccessfully = true;
                    console.log("success!");
                },
                function (data, status, headers, config) {
                    console.log("failed!");
                });
        }
        vm.resetVm = function (syllabusForm) {
            vm.syllabusVm = null;
            vm.selectedLevel = 0;
            vm.selectedTrade = 0;
            vm.selectedLanguageLists = [];
            syllabusForm.$submitted = false;
            $scope.syllabusForm.$setPristine();
            vm.isSaveDisabled = true;
            vm.savedSuccessfully = false;
        }
        vm.syllabusList = [];
        vm.getAllSyllabus = function () {
            service.get("/api/getsyllabus")
                .then(function (response) {

                    vm.syllabusList = response.data;
                    vm.syllabusList.forEach(function (item, index) {
                        var names = "";
                        for (var i = 0; i < item.LanguageShortNames.length; i++) {
                            names += item.LanguageShortNames[i];
                            if (i < item.LanguageShortNames.length - 1)
                                names += ",";
                        }
                        item.ShortNames = names;
                        item.SyllabusfilePath = "/Uploads/" + item.Syllabus.SyllabusDocUrl;
                        item.TestFilePath = "/Uploads/" + item.Syllabus.TestPlanUrl;
                    });
                    vm.isListLoaded = true;
                    vm.demoList = vm.syllabusList;
                }, function (response) {
                    console.log(response);
                });
        }
        vm.editSyllabus = function (id) {
            vm.isUpdateVisible = true;
            vm.isSaveVisible = false;
            vm.selectCreateTab();
            service.get("/api/getsinglesyllabus?id=" + id)
                .then(function (response) {
                        console.log(response.data);
                        var model = response.data;
                        vm.syllabusVm.Syllabus.SyllabusId = id;
                        vm.syllabusVm.Syllabus.SyllabusName = model.Syllabus.SyllabusName;
                        vm.syllabusVm.Syllabus.TradeId = model.Syllabus.TradeId;
                        vm.syllabusVm.Syllabus.LevelId = model.Syllabus.LevelId;
                        vm.syllabusVm.Syllabus.SyllabusDocUrl = model.Syllabus.SyllabusDocUrl;
                        vm.syllabusVm.Syllabus.TestPlanUrl = model.Syllabus.TestPlanUrl;
                        vm.syllabusVm.Syllabus.DevelopmentOfficer = model.Syllabus.DevelopmentOfficer;
                        vm.syllabusVm.Syllabus.Manager = model.Syllabus.Manager;
                        vm.syllabusVm.Syllabus.UploadBy = model.Syllabus.UploadBy;
                        vm.syllabusVm.Syllabus.UploadDate = new Date(model.Syllabus.UploadDate);
                        vm.syllabusVm.Syllabus.ActiveDate = new Date(model.Syllabus.ActiveDate);
                        vm.syllabusVm.Syllabus.Status = model.Syllabus.Status;
                        vm.selectedLevel = model.Syllabus.LevelId;
                        vm.selectedTrade = model.Syllabus.TradeId;
                        vm.selectedLanguageLists = model.SyllabusLanguages;
                        if (vm.selectedLanguageLists.length > 0)
                            vm.isUpdateDisabled = false;
                        console.log(vm.syllabusVm);
                        console.log(vm.selectedLevel);
                        console.log(vm.selectedTrade);
                    },
                    function (response) {
                        console.log(response);
                    });
        }
        vm.demoList = [];
        vm.searchSyllabus = function (levelId, tradeId) {
            vm.syllabusList = vm.demoList;

            var level = parseInt(levelId);
            var trade = parseInt(tradeId);
            if (level) {
                vm.syllabusList = vm.syllabusList.filter(x=>x.Syllabus.LevelId === level);
            }
            if (trade) {
                vm.syllabusList = vm.syllabusList.filter(x=>x.Syllabus.TradeId === trade);
            }

            console.log(vm.demoList);
            console.log(vm.syllabusList);
        }
        vm.downloadFile = function (fileName) {
            service.get("/api/download", fileName)
                .then(function (response) {
                    console.log(response);
                });
        }
        //highchart
        vm.times = [];
        vm.values = [];

        service.get("/Uploads/InputFile.csv")
            .then(function (res) {
                var rows = res.data.split(/\r?\n|\r/);
                for (var i = 1; i < rows.length - 1; i++) {
                    var cells = rows[i].split(",");
                    var nextCells = rows[i + 1].split(",");
                    //implement logic
                    var amPm = cells[0].slice(-2);
                    var timeValue = parseInt(cells[0].substring(0, cells[0].length - 2));
                    if (amPm === "PM" && timeValue!==12) {
                        timeValue += 12;
                    }
                    if (amPm === "AM" && timeValue === 12) {
                        timeValue -= 12;
                    }
                    var thershold = 500;

                    if (timeValue >= 8 && timeValue <= 17.59)
                        thershold = 1500;

                    if (parseInt(cells[1], 10) !== thershold && parseInt(nextCells[1], 10) !== thershold) {
                        vm.values.push({
                            y: parseInt(cells[1], 10),
                            segmentColor: 'red',
                            marker: {
                                symbol: 'url(https://www.highcharts.com/samples/graphics/sun.png)'
                            }
                        });
                        vm.times.push(cells[0]);
                    }
                    else if (parseInt(cells[1], 10) !== thershold) {
                        vm.values.push({
                            y: parseInt(cells[1], 10),
                            marker: {
                                symbol: 'url(https://www.highcharts.com/samples/graphics/sun.png)'
                            }
                        });
                        vm.times.push(cells[0]);
                    }
                    else {
                        vm.values.push(parseInt(cells[1], 10));
                        vm.times.push(cells[0]);
                    }
                }
                console.log(vm.times);
                console.log(vm.values);
                vm.chart(vm.times, vm.values);
            });

        vm.chart = function (times, values) {
            Highcharts.chart('chart-container', {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'cooling data info per hour'
                },
                subtitle: {
                    text: 'Source: khairul-omi.com'
                },
                xAxis: {
                    categories: times
                },
                yAxis: {
                    title: {
                        text: 'Value'
                    },
                    labels: {
                        formatter: function () {
                            return this.value + '°';
                        }
                    },
                    min: 0
                },
                tooltip: {
                    crosshairs: false,
                    shared: true
                },
                plotOptions: {
                    spline: {
                        marker: {
                            radius: 4,
                            lineColor: "#666666",
                            lineWidth: 1
                        }
                    }
                },
                series: [{
                    type: 'coloredline',
                    name: 'Data',
                    marker: {
                        symbol: 'circle'
                    },
                    data: values

                }]
            });
        }
    }


})();