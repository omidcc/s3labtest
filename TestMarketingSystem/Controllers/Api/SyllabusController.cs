﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TestMarketingSystem.Models;
using TestMarketingSystem.ViewModels;

namespace TestMarketingSystem.Controllers.Api
{

    public class SyllabusController : ApiController
    {
        S3LabTestDBEntities Db = new S3LabTestDBEntities();

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/getlanguages")]
        public IHttpActionResult Languages()
        {
            List<Language> languages = Db.Languages.ToList();
            return Ok(languages);
        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/getlevels")]
        public IHttpActionResult Levels()
        {
            List<Level> levels = Db.Levels.ToList();
            return Ok(levels);
        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/gettrades")]
        public IHttpActionResult Trades()
        {
            List<Trade> trades = Db.Trades.ToList();
            return Ok(trades);
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/upload")]
        public async Task<HttpResponseMessage> UploadFiles()
        {
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count < 1)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            foreach (string file in httpRequest.Files)
            {
                var postedFile = httpRequest.Files[file];
                var filePath = HttpContext.Current.Server.MapPath("~/Uploads/" + postedFile.FileName);
                postedFile.SaveAs(filePath);
            }
            return Request.CreateResponse(HttpStatusCode.Created);
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/syllabus")]
        public IHttpActionResult CreateSyllabus(SyllabusViewModel model)
        {
            Syllabu syllabus = new Syllabu();
            syllabus.SyllabusName = model.Syllabus.SyllabusName;
            syllabus.TradeId = model.Syllabus.TradeId;
            syllabus.LevelId = model.Syllabus.LevelId;

            syllabus.SyllabusDocUrl = model.Syllabus.SyllabusDocUrl.Substring(model.Syllabus.SyllabusDocUrl.LastIndexOf("\\", StringComparison.Ordinal) + 1);
            syllabus.TestPlanUrl = model.Syllabus.TestPlanUrl.Substring(model.Syllabus.TestPlanUrl.LastIndexOf("\\", StringComparison.Ordinal) + 1);
            syllabus.DevelopmentOfficer = model.Syllabus.DevelopmentOfficer;
            syllabus.Manager = model.Syllabus.Manager;
            syllabus.UploadBy = model.Syllabus.UploadBy;
            syllabus.UploadDate = DateTime.Now;
            syllabus.ActiveDate = model.Syllabus.ActiveDate;
            syllabus.Status = model.Syllabus.Status;

            Db.Syllabus.Add(syllabus);
            Db.SaveChanges();

            foreach (var lang in model.SyllabusLanguages)
            {
                Db.SyllabusLanguages.Add(new SyllabusLanguage()
                {
                    SyllabusId = syllabus.SyllabusId,
                    LanguageId = lang
                });
            }
            Db.SaveChanges();
            return Ok(model);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/updatesyllabus")]
        public IHttpActionResult UpdateSyllabus(SyllabusViewModel model)
        {
            Syllabu syllabus = Db.Syllabus.SingleOrDefault(x => x.SyllabusId == model.Syllabus.SyllabusId);
            SyllabusViewModel vm = new SyllabusViewModel();
            if (syllabus != null)
            {
                syllabus.SyllabusName = model.Syllabus.SyllabusName;
                syllabus.TradeId = model.Syllabus.TradeId;
                syllabus.LevelId = model.Syllabus.LevelId;
                if (syllabus.SyllabusDocUrl == model.Syllabus.SyllabusDocUrl && syllabus.TestPlanUrl == model.Syllabus.TestPlanUrl)
                {
                    vm.IsUploadAllowed = false;
                }
                syllabus.SyllabusDocUrl = model.Syllabus.SyllabusDocUrl.Substring(model.Syllabus.SyllabusDocUrl.LastIndexOf("\\", StringComparison.Ordinal) + 1);
                syllabus.TestPlanUrl = model.Syllabus.TestPlanUrl.Substring(model.Syllabus.TestPlanUrl.LastIndexOf("\\", StringComparison.Ordinal) + 1);
                syllabus.DevelopmentOfficer = model.Syllabus.DevelopmentOfficer;
                syllabus.Manager = model.Syllabus.Manager;
                syllabus.UploadBy = model.Syllabus.UploadBy;
                syllabus.UploadDate = DateTime.Now;
                syllabus.ActiveDate = model.Syllabus.ActiveDate;
                syllabus.Status = model.Syllabus.Status;

                Db.SaveChanges();

                List<SyllabusLanguage> syllabusLanguages = Db.SyllabusLanguages
                    .Where(x => x.SyllabusId == model.Syllabus.SyllabusId)
                    .ToList();
                foreach (var lang in syllabusLanguages)
                {
                    Db.SyllabusLanguages.Remove(lang);
                }
                Db.SaveChanges();
                foreach (var lang in model.SyllabusLanguages)
                {
                    Db.SyllabusLanguages.Add(new SyllabusLanguage()
                    {
                        SyllabusId = syllabus.SyllabusId,
                        LanguageId = lang
                    });
                }
                Db.SaveChanges();
            }
            return Ok(vm);
        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/getsyllabus")]
        public async Task<IHttpActionResult> GetSyllabus()
        {
            List<SyllabusViewModel> model = new List<SyllabusViewModel>();

            var syllabusList = Db.Syllabus.ToList();
            var languageLists = Db.Languages.ToList();

            foreach (var syl in syllabusList)
            {
                var shortNames = new List<string>();
                var syllabusLanguages = new List<int>();

                foreach (var lang in syl.SyllabusLanguages)
                {
                    var singleOrDefault = languageLists.SingleOrDefault(x => x.LanguageId == lang.LanguageId);
                    if (singleOrDefault != null)
                    {
                        shortNames.Add(singleOrDefault
                            .LanguageShortName);
                        syllabusLanguages.Add(singleOrDefault.LanguageId);
                    }
                }

                model.Add(new SyllabusViewModel()
                {
                    Syllabus = new Syllabu()
                    {
                        SyllabusId = syl.SyllabusId,
                        SyllabusName = syl.SyllabusName,
                        LevelId = syl.LevelId,
                        TradeId = syl.TradeId,
                        SyllabusDocUrl = syl.SyllabusDocUrl,
                        TestPlanUrl = syl.TestPlanUrl,
                        DevelopmentOfficer = syl.DevelopmentOfficer,
                        Manager = syl.Manager,
                        UploadDate = syl.UploadDate,
                        ActiveDate = syl.ActiveDate,
                        UploadBy = syl.UploadBy,
                        SyllabusLanguages = syl.SyllabusLanguages
                    },
                    LevelName = syl.Level.LevelShortName,
                    TradeName = syl.Trade.TradeCode,
                    SyllabusLanguages = syllabusLanguages,
                    LanguageShortNames = shortNames
                });
            }
            return Ok(model);
        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/getsinglesyllabus")]
        public async Task<IHttpActionResult> GetSyllabusById(int id)
        {
            var syllabus = await Db.Syllabus.FindAsync(id);
            SyllabusViewModel vm = new SyllabusViewModel();
            vm.Syllabus = syllabus;

            List<int> syllabusLanguagesId = new List<int>();

            if (syllabus != null)
                foreach (var lang in syllabus.SyllabusLanguages)
                {
                    syllabusLanguagesId.Add(lang.LanguageId);
                }
            vm.SyllabusLanguages = syllabusLanguagesId;
            return Ok(vm);
        }


    }
}